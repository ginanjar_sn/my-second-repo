<?php
    require_once('frog.php');
    require_once('Animal.php');
    require_once('ape.php');
    echo "<h1>Belajar OOP PHP</h1>";
    echo "<h3>Animal Pertama:</h3>";
    $sheep = new Animal("shaun");
    echo "Nama Animal: $sheep->name <br>";
    echo "Jumlah kaki: $sheep->legs <br>";
    echo "Hewan berdarah dingin: $sheep->cold_blooded <br>";
    echo "<h3>Animal Kedua:</h3>";
    $kodok = new frog("buduk");
    echo "Nama Animal: $kodok->name <br>";
    echo "Jumlah kaki: $kodok->legs <br>";
    echo "Hewan berdarah dingin: $kodok->cold_blooded <br>";
    echo "Kemampuan: ";
    echo $kodok->jump();
    echo "<h3>Animal Ketiga:</h3>";
    $sungokong = new ape("kera sakti");
    echo "Nama Animal: $sungokong->name <br>";
    echo "Jumlah kaki: $sungokong->legs <br>";
    echo "Hewan berdarah dingin: $sungokong->cold_blooded <br>";
    echo "Kemampuan: ";
    echo $sungokong->yell();


?>